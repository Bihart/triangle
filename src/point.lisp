(in-package :cl-user)
(defpackage :point
  (:use :cl)
  (:export
   :make-point :point :middle-point :points->linea-aux :point->array)
  (:import-from :utils :mean)
  (:import-from :utils :minmax))
(in-package :point)

(defstruct point
  (x (random 2.0) :type single-float :read-only t)
  (y (random 2.0) :type single-float :read-only t))

(defun point->array (point)
  (with-slots (x y) point
    (make-array 2 :initial-contents (list x y))))

(defun slope-aux (p1 p2 &optional (horizontal t))
  (let ((l1 (point->array p1))
        (l2 (point->array p2)))
    (reduce #'/ (funcall (if horizontal #'reverse #'identity)
                         (map 'vector #'- l1 l2)))))

(defun cal-slope (p1 p2)
  "A function that pretend calculate the slope of the a line using two
points `p1' and `p2'."
  (labels ((equal-x (p1 p2) (= (point-x p1) (point-x p2))))
    (if (equal-x p1 p2)
        (values (slope-aux p1 p2 nil) :y)
        (values (slope-aux p1 p2)     :x))))

(defun cal-independent-term (slope p1)
  (- (point-y p1) (* slope (point-x p1))))

(defun points->linea-aux (p1 p2)
  (multiple-value-bind (slope indep-var) (cal-slope p1 p2)
    (let ((inter (cal-independent-term slope p1)))
      (list :m slope :b inter :iv indep-var))))

(defun middle-point (&rest points)
  (make-point
   :x (mean (mapcar #'point-x points))
   :y (mean (mapcar #'point-y points))))

(defun bounds (&rest points)
  (let ((bounds-x (minmax (mapcar #'point-x points)))
        (bounds-y (minmax (mapcar #'point-y points))))
    (make-array 2 :initial-contents (list bounds-x bounds-y))))
