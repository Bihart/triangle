(in-package :cl-user)
(defpackage :triangle
  (:use :cl)
  (:export :triangle :make-triangle :point-inside-p :next-point)
  (:import-from :line :get-pos-relation)
  (:import-from :line :get-all-lines)
  (:import-from :point :make-point)
  (:import-from :point :middle-point)
  (:import-from :point :point)
  (:import-from :utils :rand-nth))
(in-package :triangle)

(defstruct triangle
  (A (make-point :x 0.0 :y 0.0) :type point :read-only t)
  (B (make-point :x 1.0 :y (sqrt 3)) :type point :read-only t)
  (C (make-point :x 2.0 :y 0.0) :type point :read-only t))

(defun point-inside-p (tr p)
  (labels
      ((get-pos-relations (lines p)
         (mapcar #'(lambda (line) (get-pos-relation line p)) lines)))
    (let* ((D (get-inside-point tr))
           (lines (with-slots (A B C) tr (get-all-lines A B C)))
           (pos-correct-relations (get-pos-relations lines D))
           (pos-relations (get-pos-relations lines p)))
      (equal pos-correct-relations pos-relations))))

(defun random-vertex (tr)
  (with-slots (A B C) tr (rand-nth A B C)))

(defun next-point (tr p)
  (middle-point p (random-vertex tr)))

(defun get-inside-point (triangle)
  (with-slots (A B C) triangle
    (middle-point A B C)))
