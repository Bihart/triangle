(in-package :cl-user)
(defpackage :line
  (:use :cl)
  (:export :make-line :get-pos-realtion :points->line)
  (:import-from :point :points->linea-aux)
  (:import-from :point :point->array))
(in-package :line)

(defstruct line
  (m  nil :type single-float :read-only t)
  (b  nil :type single-float :read-only t)
  (iv nil :type keyword :read-only t))

(defun get-lines-equiation (line)
  "A line's equation  0 = `m'`iv' + `b' - `dv'."
  (with-slots (m b) line
    (lambda (iv dv)
      (- dv b (* m iv)))))

(defun eval-lines-equiation (line p)
  (let* ((iv (line-iv line))
         (le (get-lines-equiation line))
         (args (case iv
                 (:x (point->array p))
                 (:y (reverse (point->array p)))
                 (otherwise nil))))
    (apply le (map 'list #'identity args))))

(defun get-pos-relation (line p)
  (let ((ans (eval-lines-equiation line p)))
    (cond ((plusp ans)  :GT)
          ((minusp ans) :LT)
          ((zerop ans)  :IN)
          (t nil))))

(defun points->line (p1 p2)
  (apply #'line:make-line (points->linea-aux p1 p2)))

(defun get-all-lines (&rest points)
  (loop :for lp :on points
        :with p1 = (first lp)
        :append (loop :for r-lp :on (rest lp)
                      :with p2 = (first r-lp)
                      :collect (points->line p1 p2))))
