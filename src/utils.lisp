(in-package :cl-user)
(defpackage :utils
  (:use :cl)
  (:export :mean :minmax :rand-nth))
(in-package :utils)

(defun mean (list)
  (/ (reduce #'+ list) (length list)))

(defun minmax (list)
  (let ((min (reduce #'min list))
        (max (reduce #'max list)))
    (make-array 2 :initial-contents (list min max))))

(defun random-uniform (&key (low 0.0) (high 1.0))
  (+ low (random (+ single-float-epsilon (- high low)))))

(defun rand-nth (&rest lst)
  (nth (random (length lst)) lst))

(defun eucl-dist (lst1 lst2)
  (labels ((diff (v1 v2) (map 'vector #'- v1 v2))
           (pow2 (lst)   (map 'vector #'(lambda (x) (* x x)) lst))
           (sum  (lst)   (reduce #'+ lst)))
    (sqrt (sum (pow2 (diff lst1 lst2))))))
