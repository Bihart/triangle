(in-package :cl-user)
(defpackage :my-tests
  (:use :cl
   :fiveam :recursion))
(in-package :my-tests)

(def-suite test-calculate-slope-with-two-points
  :description "Test my system"
  :in my-system)

(in-suite my-system)

(test my-test
  "my test because yes"
  (is (= 2 (+ 1 1)))
  (is (= 0 (+ 0 0)))
  (is (= 1/2 (+ 1/4 1/4))) :in 'my-system)


(run! 'my-system)
